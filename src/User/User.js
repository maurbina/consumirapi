import React, { Component } from "react";
import "./user.css";

export class User extends Component {
  render() {
    return (
      <div
        className="target"
        onClick={() => {
          this.props.onClick(this.props.first, this.props.last);
        }}
      >
        <p className="nameUser">
          {this.props.first} {this.props.last}
        </p>
        <p className="emailUser">{this.props.email}</p>
      </div>
    );
  }
}

export default User;
