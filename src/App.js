import React, { Component } from "react";
import User from "./User/User";
import "./App.css";
import Test from "./detail/Detail";

class EmpleadoUser extends Component {
  state = {
    users: [],
    usersDet: {},
    showUserDetail: false,
    name: "",
    lastName: ""
  };

  componentWillMount() {
    fetch("https://randomapi.com/api/6de6abfedb24f889e0b5f675edc50deb")
      .then(response => response.json())
      .then(usersIn => {
        usersIn.results[0].forEach(user => {
          let data = {
            first: user.first,
            last: user.last,
            email: user.email
          };
          // console.log(data);
          this.setState({ users: this.state.users.concat([data]) });
        });
      });

    fetch("https://randomuser.me/api/")
      .then(response => response.json())
      .then(usersDetail => {
        let data2 = {
          name:
            usersDetail.results[0].name.first +
            usersDetail.results[0].name.last,
          phone: usersDetail.results[0].phone,
          picture: usersDetail.results[0].picture.thumbnail, 
          street : usersDetail.results[0].location.street
        };
        console.log(data2);
        this.setState({ usersDet: data2 });
      });
  }
  clickHandler = (name, lastName) => {
    this.setState({
      showUserDetail: !this.state.showUserDetail,
      name: name,
      lastName: lastName
    });
  };
  render() {
    if (this.state.users.length > 0) {
      let hiddenWiki = null;
      let active = null;
      let userSh = this.state.showUserDetail;

      if (userSh) {
        hiddenWiki = (
          <Test
            name={this.state.name + " " + this.state.lastName}
            phone={this.state.usersDet.phone}
            picture={this.state.usersDet.picture}
            street={this.state.usersDet.street}
          />
        );

        active = "hiddenWiki";
      }

      return (
        <div className="content">
          {hiddenWiki}
          <div className="container">
            <div className={active}>
              {this.state.users.map((user, i) => (
                <User
                  first={user.first}
                  last={user.last}
                  email={user.email}
                  key={i}
                  onClick={(e, b) => {
                    this.clickHandler(e, b);
                  }}
                />
              ))}
            </div>
          </div>
        </div>
      );
    } else {
      return <center>Cargando Contactos</center>;
    }
  }
}

export default EmpleadoUser;
