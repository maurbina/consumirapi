import React, { Component } from "react";
import "./detail.css";

class Test extends Component {
  render() {
    return (
      <div className="container">
        <div className="imgName">
          <div className="imgleft">
            <img src={this.props.picture} alt="img" />
          </div>
          <div className="name">
            <p>{this.props.name}</p>
          </div>
        </div>
        <div className="direction">
          <div>
            <p>Add to Favorite</p>
          </div>
          <div>
            <p>Celular : {this.props.phone}</p>
          </div>
          <div>
            <p>{this.props.street}</p>
          </div>
        </div>
      </div>
    );
  }
}

export default Test;
